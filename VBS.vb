﻿Module Module1
    Sub Main()
        Dim index As Integer 'Se declara un indice para el for, que recorrerá las calificaciones
        Dim calificacion1 As Integer ' se declara una calificacion, quien obtendra la calificacion en cada iteracion
        Dim total As Double ' Suma y resultado del promedio 
        For index = 1 To 5 ' se itera las 5 calificaciones
            Do ' se usa para validacion de calificaciones y si te equivocas , vuelvas a ingresar
                Try ' por si incluyes un elemnto invalido lanza una exepcion
                    calificacion1 = InputBox("Ingrese calificacion " & index, "ITO")
                    total += calificacion1
                Catch ex As Exception
                    MsgBox("Entrada Invalida")
                End Try
            Loop While calificacion1 <= 0 Or calificacion1 >= 11
        Next
        total = total / (index - 1)
        MsgBox(total)
    End Sub
End Module
